#include "Profile.h"

const Glib::ustring &Profile::getName() const {
    return name;
}

void Profile::setName(Glib::ustring name) {
    this->name = std::move(name);
}

const Glib::ustring &Profile::getExecutable() const {
    return executable;
}

void Profile::setExecutable(Glib::ustring executable) {
    this->executable = std::move(executable);
}

std::list<ProfileOption_ptr> &Profile::getOptions() {
    return this->options;
}

void Profile::addOption(ProfileOption_ptr option) {
    this->options.emplace_back(option);
}

void Profile::setOptions(std::list<ProfileOption_ptr> options) {
    this->options = std::move(options);
}

bool Profile::getIsUsingPrime() const {
    return isUsingPrime;
}

void Profile::setIsUsingPrime(bool isUsingPrime) {
    Profile::isUsingPrime = isUsingPrime;
}

const Glib::ustring &Profile::getPrimeDriverName() const {
    return primeDriverName;
}

void Profile::setPrimeDriverName(const Glib::ustring &primeDriverName) {
    Profile::primeDriverName = primeDriverName;
}

const Glib::ustring &Profile::getDevicePCIId() const {
    return devicePCIId;
}

void Profile::setDevicePCIId(const Glib::ustring &devicePCIId) {
    Profile::devicePCIId = devicePCIId;
}

std::map<Glib::ustring, Glib::ustring> Profile::getOptionsAsMap() {
    std::map<Glib::ustring, Glib::ustring> optionMap;

    for(auto & option : this->options) {
        optionMap[option->getName()] = option->getValue();
    }

    return optionMap;
}
