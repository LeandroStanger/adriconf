#include "OptionSpin.h"

OptionSpin::OptionSpin(const ProfileOption_ptr &optionPtr, int validStartValue, int validEndValue) : SpinButton(), optionPtr(optionPtr) {
    this->set_visible(true);
    this->set_numeric(true);

    auto adjustment = Gtk::Adjustment::create(
            std::stof(this->optionPtr->getValue()),
            validStartValue,
            validEndValue,
            1,
            10
    );

    this->set_adjustment(adjustment);

    this->signal_changed().connect(sigc::mem_fun(this, &OptionSpin::onOptionChanged));
}

void OptionSpin::onOptionChanged() {
    this->optionPtr->setValue(std::to_string(int (this->get_value())));
}


