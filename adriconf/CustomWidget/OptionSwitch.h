#ifndef ADRICONF_OPTIONSWITCH_H
#define ADRICONF_OPTIONSWITCH_H

#include "gtkmm.h"
#include "../ValueObject/ProfileOption.h"

class OptionSwitch : public Gtk::Switch {
private:
    ProfileOption_ptr optionPtr;

public:
    OptionSwitch(const ProfileOption_ptr& optionPtr);

    void onOptionChanged();
};


#endif //ADRICONF_OPTIONSWITCH_H
